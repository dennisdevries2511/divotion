import { useContext, computed } from '@nuxtjs/composition-api'
import { TProductCategoryList, TProductCategoryMap, TProductList } from '~/types'

export default function useProducts() {
  const { store } = useContext()

  const products = computed<TProductList>(() => store.state.products.items)
  const categories = computed<TProductCategoryList>(() => store.getters['products/categories'])
  const productCategoryMap = computed<TProductCategoryMap>(() => store.getters['products/productCategoryMap'])

  // No longer needed as the store is pre-populated using nuxtServerInit
  // https://nuxtjs.org/docs/2.x/directory-structure/store#the-nuxtserverinit-action

  // const fetchProducts = async () => {
  //   await store.dispatch('products/fetchProducts')
  // }

  // onMounted(fetchProducts)

  return {
    products,
    categories,
    productCategoryMap,
  }
}
