import { ActionTree, MutationTree, GetterTree } from 'vuex'
import { IProduct, TProductCategoryList, TProductCategoryMap, TProductList } from '~/types'

export const state = () => ({
  items: [] as TProductList,
})

export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, any> = {
  categories(state): TProductCategoryList {
    // Unique product categories sorted alphabetically in ascending order
    return [...new Set(state.items.map(({ type }) => type))].sort()
  },
  productCategoryMap(state, { categories }): TProductCategoryMap {
    const map: TProductCategoryMap = {}

    categories.forEach((c: IProduct['type']) => {
      map[c] = state.items.filter(({ type }: IProduct) => type === c)
    })

    return map
  },
}

export const mutations: MutationTree<RootState> = {
  setProducts(state, products: TProductList) {
    state.items = [...products]
  },
}

export const actions: ActionTree<RootState, RootState> = {
  async fetchProducts({ commit }): Promise<TProductList | []> {
    commit('setLoading', true, { root: true })

    try {
      // Simulate fetching data from API
      const data = await require('@/assets/products.json')

      // Update state
      commit('setProducts', data)

      return data
    } catch (error) {
      return []
    } finally {
      commit('setLoading', false, { root: true })
    }
  },
}
