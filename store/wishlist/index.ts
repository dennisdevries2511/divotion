import { ActionTree, MutationTree, GetterTree } from 'vuex'
import { IProduct, IWishlistItem, TWishlist } from '~/types'

export const state = () => ({
  items: [] as TWishlist,
})

export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, any> = {
  numItems(state): number {
    return state.items.length
  },
}

export const mutations: MutationTree<RootState> = {
  addToWishlist(state, item: IProduct) {
    // First, check if the item already exists
    // Do not use `includes()` as the quantity could have been changed
    const index = state.items.findIndex(({ id }) => item.id === id)

    // NEVER modify the state directly
    const newList = [...state.items]

    // If it exists, increase the quantity by one
    if (index > -1) newList[index].quantity =+ 1
    // else, add it to the list
    else newList.push({ ...item, quantity: 1 })

    // Update the wishlist with the new list
    state.items = [...newList]
  },
  removeFromWishlist(state, id: IProduct['id']) {
    // First, check if the item exists
    const index = state.items.findIndex((item) => item.id === id)

    // Do nothing if the item does not exist
    if (index === -1) return

    // NEVER modify the state directly
    const newList = [...state.items]

    // Remove the item
    newList.splice(index, 1)

    // Update the wishlist with the new list
    state.items = [...newList]
  },
  updateWishlistItem(state, item: IWishlistItem) {
    // Find which item to update
    const index = state.items.findIndex(({ id }) => item.id === id)

    // Do nothing if the item does not exist
    if (index === -1) return

    // NEVER modify the state directly
    const newList = [...state.items]

    // Update the item
    newList[index] = { ...newList[index], ...item }

    // Update the wishlist with the new list
    state.items = [...newList]
  },
}

export const actions: ActionTree<RootState, RootState> = {
  async addToWishlist({ commit }, item: IProduct) {
    commit('setLoading', true, { root: true })

    // Simulate API call
    await setTimeout(() => {
      commit('addToWishlist', item)
      commit('setLoading', false, { root: true })
  }, 100)
  },
  async removeFromWishlist({ commit }, item: IProduct) {
    commit('setLoading', true, { root: true })

    // Simulate API call
    await setTimeout(() => {
      commit('removeFromWishlist', item)
      commit('setLoading', false, { root: true })
    }, 100)
  },
  async updateWishlistItem({ commit }, item: IProduct) {
    commit('setLoading', true, { root: true })

    // Simulate API call
    await setTimeout(() => {
      commit('updateWishlistItem', item)
      commit('setLoading', false, { root: true })
    }, 100)
  },
}
