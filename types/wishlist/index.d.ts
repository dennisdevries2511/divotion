import { IProduct } from "~/types";

export interface IWishlistItem extends IProduct {
  quantity: number
}

export type TWishlist = Array<IWishlistItem>
