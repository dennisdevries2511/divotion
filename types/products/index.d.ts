export interface IProduct {
  id: string
  title: string
  type: string
  discription: string
  filename: string
  height: number
  width: number
  price: number
  rating: number
}

export type TProductList = Array<IProduct>

export type TProductCategoryList = Array<IProduct['type']>

export type TProductCategoryMap = {
  [key: string]: IProduct[]
}
